﻿namespace Atonal.BowlingScore
{
    interface IRoll
    {
        RollKey RollKey { get; }
        int PinfallCount { get; }
        Roll NextRoll { get; }
    }
}
