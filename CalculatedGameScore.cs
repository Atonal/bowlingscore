﻿using System;
using System.Collections.Generic;

namespace Atonal.BowlingScore
{
    sealed class CalculatedGameScore
    {
        public static readonly int PerfectGameScore = 300;

        public readonly int CurrentGameScore;
        public readonly IReadOnlyDictionary<int, int> ScoreHistoryByFrame;

        public CalculatedGameScore(int currentGameScore, IReadOnlyDictionary<int, int> scoreHistoryByFrame)
        {
            if (currentGameScore < 0 || PerfectGameScore < currentGameScore)
            {
                throw new ArgumentOutOfRangeException(nameof(currentGameScore));
            }
            if (scoreHistoryByFrame == null)
            {
                throw new ArgumentNullException(nameof(scoreHistoryByFrame));
            }

            this.CurrentGameScore = currentGameScore;
            this.ScoreHistoryByFrame = scoreHistoryByFrame;
        }
    }
}
