﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Atonal.BowlingScore
{
    struct RollKey : IEquatable<RollKey>, IComparable<RollKey>
    {
        public static readonly RollKeysForFrames FirstRolls;
        public static readonly RollKeysForFrames SecondRolls;
        public static readonly RollKey ExtraSecondRoll;
        public static readonly RollKey BonusThirdRoll;
        public static readonly RollKey GameEnded;

        public readonly int Frame;
        public readonly RollKeyType RollType;

        RollKey(int frame, RollKeyType rollType)
        {
            Debug.Assert(GameConstants.BeginFrame <= frame && frame <= GameConstants.EndFrame);
            Debug.Assert(RollKeyType.Invalid < rollType && rollType < RollKeyType._MAX);
            this.Frame = frame;
            this.RollType = rollType;
        }

        static RollKey()
        {
            var firstRolls = new Dictionary<int, RollKey>();
            var secondRolls = new Dictionary<int, RollKey>();
            for (int frame = GameConstants.BeginFrame; frame <= GameConstants.EndFrame; frame++)
            {
                firstRolls.Add(frame, new RollKey(frame, RollKeyType.FirstRoll));
                secondRolls.Add(frame, new RollKey(frame, RollKeyType.SecondRoll));
            }
            FirstRolls = new RollKeysForFrames(firstRolls);
            SecondRolls = new RollKeysForFrames(secondRolls);
            ExtraSecondRoll = new RollKey(10, RollKeyType.ExtraSecondRoll);
            BonusThirdRoll = new RollKey(10, RollKeyType.BonusThirdRoll);
            GameEnded = new RollKey(10, RollKeyType.GameEnded);
        }

        public bool IsLastFrame => (Frame == GameConstants.EndFrame);
        public bool IsFirstRoll => (RollType == RollKeyType.FirstRoll);
        public bool IsSpearTry => (RollType == RollKeyType.SecondRoll);
        public bool IsExtraSecodRoll => (RollType == RollKeyType.ExtraSecondRoll);
        public bool IsBonusThirdRoll => (RollType == RollKeyType.BonusThirdRoll);

        public bool Equals(RollKey other)
        {
            return Frame == other.Frame
                && RollType == other.RollType;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (obj is RollKey other)
            {
                return Equals(other);
            }
            return false;
        }

        public override int GetHashCode()
        {
            Debug.Assert((int)RollKeyType.GameEnded < (1 << 4));
            return (Frame << 4) + (int)RollType;
        }

        public int CompareTo(RollKey other)
        {
            if (Frame < other.Frame)
            {
                return -1;
            }
            else if (other.Frame < Frame)
            {
                return 1;
            }

            return Compare(RollType, other.RollType);
        }

        static int Compare(RollKeyType a, RollKeyType b)
        {
            return (int)a - (int)b;
        }

        public override string ToString()
        {
            return $"Frame: {Frame} {RollType}";
        }
    }
}
