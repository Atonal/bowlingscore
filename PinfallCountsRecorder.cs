﻿using System;
using System.Collections.Generic;

namespace Atonal.BowlingScore
{
    sealed class PinfallCountsRecorder
    {
        List<Roll> recordedRolls = new List<Roll>();

        public RollKey CurrentRoll { get; private set; } = RollKey.FirstRolls[GameConstants.BeginFrame];
        public bool IsPlaying => (CurrentRoll.RollType != RollKeyType.GameEnded);
        public int CurrentStandingPinCount { get; private set; } = GameConstants.InitialPinCount;

        public void Record(int pinfallCount)
        {
            if (pinfallCount < 0 || CurrentStandingPinCount < pinfallCount)
            {
                throw new ArgumentOutOfRangeException(nameof(pinfallCount));
            }
            if (!IsPlaying)
            {
                throw new InvalidOperationException();
            }

            var rollInRecording = new Roll(CurrentRoll, pinfallCount);
            var recentRoll = GetRecentRollOrNull();
            if (recentRoll != null)
            {
                recentRoll.NextRoll = rollInRecording;
            }
            recordedRolls.Add(rollInRecording);

            var standingPinCount = CurrentStandingPinCount - pinfallCount;
            var nextState = NextRollDecision.Decide(CurrentRoll, standingPinCount);
            CurrentRoll = nextState.RollKey;
            CurrentStandingPinCount = nextState.PinCount;
        }

        Roll GetRecentRollOrNull()
        {
            if (recordedRolls.Count == 0) { return null; }

            return recordedRolls[recordedRolls.Count - 1];
        }

        public PinfallCountRecords GetRecords()
        {
            return new PinfallCountRecords(recordedRolls);
        }
    }
}
