﻿namespace Atonal.BowlingScore
{
    public static class GameConstants
    {
        //TODO: 핀 숫자 말고는 책임 범위를 RollCursor 안쪽으로 한정 짓게 바꿔야 할 듯
        public static readonly int InitialPinCount = 10;
        public static readonly int BeginFrame = 1;
        public static readonly int EndFrame = 10;
        public static readonly int BeginRollIndex = 1;
        public static readonly int EndRollIndex = 3;
    }
}
