﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Atonal.BowlingScore
{
    sealed class ScoreCalculator
    {
        readonly PinfallCountRecords records;

        public ScoreCalculator(PinfallCountRecords records)
        {
            this.records = records ?? throw new ArgumentNullException(nameof(records));
        }

        public static CalculatedGameScore Calculate(PinfallCountRecords records)
        {
            var calculator = new ScoreCalculator(records);
            return calculator.Calculate();
        }

        public CalculatedGameScore Calculate()
        {
            var pinfalls = CreateZeroScores();
            foreach (var roll in records)
            {
                pinfalls[roll.RollKey.Frame] += roll.PinfallCount;
            }

            int scorePartialSum = 0;
            var scoreHistory = new Dictionary<int, int>();
            var isFrameScoreFixed = true;
            for (int frame = GameConstants.BeginFrame; frame <= GameConstants.EndFrame; frame++)
            {
                scorePartialSum += pinfalls[frame];
                if (IsSpareBonus(frame))
                {
                    var spareRoll = records.TryGetRoll(RollKey.SecondRolls[frame]);
                    var bonusRoll = spareRoll?.NextRoll;
                    if (bonusRoll != null)
                    {
                        scorePartialSum += bonusRoll.PinfallCount;
                    }
                    else
                    {
                        isFrameScoreFixed = false;
                    }
                }
                else if (IsStrikeBonus(frame))
                {
                    var strikeRoll = records.TryGetRoll(RollKey.FirstRolls[frame]);
                    var firstBonusRoll = strikeRoll?.NextRoll;
                    var secondBonusRoll = firstBonusRoll?.NextRoll;
                    if (firstBonusRoll != null && secondBonusRoll != null)
                    {
                        scorePartialSum += firstBonusRoll.PinfallCount + secondBonusRoll.PinfallCount;
                    }
                    else if (firstBonusRoll != null)
                    {
                        scorePartialSum += firstBonusRoll.PinfallCount;
                        isFrameScoreFixed = false;
                    }
                    else
                    {
                        isFrameScoreFixed = false;
                    }
                }

                if (isFrameScoreFixed)
                {
                    scoreHistory.Add(frame, scorePartialSum);
                }
            }
            return new CalculatedGameScore(scorePartialSum, scoreHistory);
        }

        bool IsSpareBonus(int frame)
        {
            Debug.Assert(GameConstants.BeginFrame <= frame && frame <= GameConstants.EndFrame);

            if (frame == GameConstants.EndFrame) { return false; }

            var firstRoll = records.TryGetPinfallCount(RollKey.FirstRolls[frame]);
            var secondRoll = records.TryGetPinfallCount(RollKey.SecondRolls[frame]);
            return (firstRoll != GameConstants.InitialPinCount)
                && ((firstRoll + secondRoll) == GameConstants.InitialPinCount);
        }

        bool IsStrikeBonus(int frame)
        {
            Debug.Assert(GameConstants.BeginFrame <= frame && frame <= GameConstants.EndFrame);

            if (frame == GameConstants.EndFrame) { return false; }
            return records.TryGetPinfallCount(RollKey.FirstRolls[frame]) == GameConstants.InitialPinCount;
        }

        static Dictionary<int, int> CreateZeroScores()
        {
            var scores = new Dictionary<int, int>();
            for (int frame = GameConstants.BeginFrame; frame <= GameConstants.EndFrame; frame++)
            {
                scores.Add(frame, 0);
            }
            return scores;
        }
    }
}
