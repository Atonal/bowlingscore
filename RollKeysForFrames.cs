﻿using System;
using System.Collections.Generic;

namespace Atonal.BowlingScore
{
    sealed class RollKeysForFrames
    {
        readonly IReadOnlyDictionary<int, RollKey> frameToRoll;

        public RollKeysForFrames(IDictionary<int, RollKey> frameToRoll)
        {
            var frameCount = GameConstants.EndFrame - GameConstants.BeginFrame + 1;
            if (frameToRoll.Count != frameCount)
            {
                throw new ArgumentException(nameof(frameToRoll));
            }
            for (int frame = GameConstants.BeginFrame; frame <= GameConstants.EndFrame; frame++)
            {
                if (frameToRoll.TryGetValue(frame, out var value))
                {
                    if (value.Frame != frame)
                    {
                        throw new ArgumentException(nameof(frameToRoll));
                    }
                }
                else
                {
                    throw new ArgumentException(nameof(frameToRoll));
                }
            }
            this.frameToRoll = new Dictionary<int, RollKey>(frameToRoll);
        }

        public RollKey this[int frame]
        {
            get
            {
                if (frame < GameConstants.BeginFrame || GameConstants.EndFrame < frame)
                {
                    throw new ArgumentOutOfRangeException(nameof(frame));
                }
                return frameToRoll[frame];
            }
        }
    }
}
