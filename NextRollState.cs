﻿namespace Atonal.BowlingScore
{
    sealed class NextRollState
    {
        public readonly RollKey RollKey;
        public readonly int PinCount;

        public NextRollState(RollKey roll, int pinCount)
        {
            this.RollKey = roll;
            this.PinCount = pinCount;
        }
    }
}
