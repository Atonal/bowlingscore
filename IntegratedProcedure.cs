﻿using System;
using System.Diagnostics;

namespace Atonal.BowlingScore
{
    sealed class IntegratedProcedure
    {
        PinfallCountsRecorder recorder;

        public static void RunLoop()
        {
            var instance = new IntegratedProcedure();
            instance.Loop();
        }

        void Loop()
        {
            while (true)
            {
                recorder = new PinfallCountsRecorder();
                WriteNewGameNotice();
                RunGame();

                //TODO: 종료 후 재개 처리
            }
        }

        static void WriteNewGameNotice()
        {
            Console.WriteLine("새 게임을 시작합니다.");
            Console.WriteLine();
        }

        void RunGame()
        {
            while (recorder.IsPlaying)
            {
                PrintCurrentRollInfo();
                var pinfalls = ReadPinfalls();
                recorder.Record(pinfalls);
                var records = recorder.GetRecords();
                var score = ScoreCalculator.Calculate(records);
                PrintScore(score);
            }
        }

        void PrintCurrentRollInfo()
        {
            Console.Write($"[{recorder.CurrentRoll.Frame} 프레임] ");
            if (recorder.CurrentRoll.IsFirstRoll)
            {
                Console.WriteLine("첫번째 투구");
            }
            else if (recorder.CurrentRoll.IsSpearTry)
            {
                Console.WriteLine("스페어 시도");
            }
            else if (recorder.CurrentRoll.IsExtraSecodRoll || recorder.CurrentRoll.IsBonusThirdRoll)
            {
                Console.WriteLine("보너스 투구");
            }
            else
            {
                Debug.Assert(false);
            }
        }

        int ReadPinfalls()
        {
            var currentPinCount = recorder.CurrentStandingPinCount;
            Console.WriteLine($"몇 개의 핀이 쓰러졌나요? (현재 핀의 수: {currentPinCount})");
            while (true)
            {
                Console.Write("> ");
                var input = Console.ReadLine();
                if (int.TryParse(input, out var parsedInput))
                {
                    if (0 <= parsedInput && parsedInput <= currentPinCount)
                    {
                        Console.WriteLine();
                        return parsedInput;
                    }
                    else
                    {
                        Console.WriteLine("입력 오류: 허용 되지 않은 범위의 값이 입력되었습니다.");
                        Console.WriteLine();
                    }
                }
                else
                {
                    Console.WriteLine("입력 오류: 올바른 정수 형식의 값이 아닙니다.");
                    Console.WriteLine();
                }
            }
        }

        static void PrintScore(CalculatedGameScore score)
        {
            Console.WriteLine($"현재 점수: {score.CurrentGameScore}");
            //TODO: 프레임별 점수 히스토리 미구현
            Console.WriteLine();
        }
    }
}
