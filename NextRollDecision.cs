﻿using System;
using System.Diagnostics;

namespace Atonal.BowlingScore
{
    sealed class NextRollDecision
    {
        readonly int standingPinCount;
        readonly RollKey roll;

        public static NextRollState Decide(RollKey roll, int standingPinCount)
        {
            var decision = new NextRollDecision(roll, standingPinCount);
            return decision.Decide();
        }

        NextRollDecision(RollKey roll, int standingPinCount)
        {
            if (standingPinCount < 0 || GameConstants.InitialPinCount < standingPinCount)
            {
                throw new ArgumentOutOfRangeException(nameof(standingPinCount));
            }
            if (roll.RollType == RollKeyType.GameEnded)
            {
                throw new ArgumentOutOfRangeException(nameof(roll));
            }

            this.standingPinCount = standingPinCount;
            this.roll = roll;
        }

        bool HasStandingPin => (standingPinCount > 0);

        NextRollState Decide()
        {
            if (!roll.IsLastFrame)
            {
                if (roll.IsFirstRoll)
                {
                    if (HasStandingPin)
                    {
                        return CreateSpearTry();
                    }
                    else
                    {
                        return CreateNextNewFrame();
                    }
                }
                else if (roll.IsSpearTry)
                {
                    return CreateNextNewFrame();
                }
                else
                {
                    Debug.Assert(false);
                }
            }
            else
            {
                if (roll.IsFirstRoll)
                {
                    if (HasStandingPin)
                    {
                        return CreateSpearTry();
                    }
                    else
                    {
                        return CreateExtraSecondBonus();
                    }
                }
                else if (roll.IsSpearTry)
                {
                    if (HasStandingPin)
                    {
                        return CreateGameEnded();
                    }
                    else
                    {
                        return CreateBonusThirdRoll();
                    }
                }
                else if (roll.IsExtraSecodRoll)
                {
                    return CreateBonusThirdRoll();
                }
                else if (roll.IsBonusThirdRoll)
                {
                    return CreateGameEnded();
                }
                else
                {
                    Debug.Assert(false);
                }
            }

            Debug.Assert(false);
            throw new Exception();
        }

        NextRollState CreateNextNewFrame()
        {
            return new NextRollState(
                RollKey.FirstRolls[roll.Frame + 1],
                GameConstants.InitialPinCount);
        }

        NextRollState CreateSpearTry()
        {
            return new NextRollState(
                RollKey.SecondRolls[roll.Frame],
                standingPinCount);
        }

        NextRollState CreateExtraSecondBonus()
        {
            return new NextRollState(
                RollKey.ExtraSecondRoll,
                GameConstants.InitialPinCount);
        }

        NextRollState CreateGameEnded()
        {
            return new NextRollState(
                RollKey.GameEnded,
                GameConstants.InitialPinCount);
        }

        NextRollState CreateBonusThirdRoll()
        {
            return new NextRollState(
                RollKey.BonusThirdRoll,
                GameConstants.InitialPinCount);
        }
    }
}
