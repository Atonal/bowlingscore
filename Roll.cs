﻿namespace Atonal.BowlingScore
{
    sealed class Roll : IRoll
    {
        public RollKey RollKey { get; }
        public int PinfallCount { get; }
        public Roll NextRoll { get; set; }

        public Roll(RollKey rollKey, int pinfallCount)
        {
            this.RollKey = rollKey;
            this.PinfallCount = pinfallCount;
        }
    }
}
