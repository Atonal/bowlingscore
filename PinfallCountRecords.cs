﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Atonal.BowlingScore
{
    sealed class PinfallCountRecords : IEnumerable<IRoll>
    {
        readonly SortedDictionary<RollKey, IRoll> rollRecords;

        public PinfallCountRecords(IEnumerable<IRoll> rollRecords)
        {
            if (rollRecords == null)
            {
                throw new ArgumentNullException(nameof(rollRecords));
            }

            this.rollRecords = new SortedDictionary<RollKey, IRoll>();
            foreach (var record in rollRecords)
            {
                this.rollRecords.Add(record.RollKey, record);
            }
        }

        public IEnumerator<IRoll> GetEnumerator()
        {
            return rollRecords.Values.GetEnumerator();
        }

        public int TryGetPinfallCount(RollKey rollKey)
        {
            return TryGetRoll(rollKey)?.PinfallCount ?? 0;
        }

        public IRoll TryGetRoll(RollKey rollKey)
        {
            if (rollRecords.TryGetValue(rollKey, out var roll))
            {
                return roll;
            }
            return null;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
