﻿namespace Atonal.BowlingScore
{
    enum RollKeyType
    {
        Invalid,
        FirstRoll,
        SecondRoll,
        ExtraSecondRoll,
        BonusThirdRoll,
        GameEnded,
        _MAX
    }
}
